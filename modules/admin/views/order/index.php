<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'created_at',
                'format' => 'date',
            ],
            [
                'attribute' => 'updated_at',
                'format' => 'date',
            ],
            'qty',
            [
                'attribute' => 'sum',
                'value' => function ($data) {
                    return floor($data->sum/100).' руб. '.($data->sum - (floor($data->sum/100)*100)).' коп.';
                },
            ],
            [
                'attribute' => 'status',
                'value' => function ($data) {
                    return $data->status == 0 ? 'Открыт' : 'Закрыт';  
                },
                'filter' => ['0' => 'Открыт', '1' => 'Закрыт'],
            ],
            [
                'value' => function ($data) {
                    return Html::a('<i class="fa fa-folder-open-o" aria-hidden="true"></i>', ['update', 'id' => $data->id], [
                                'class' => 'btn btn-success btn-sm',
                                'data' => [
                                    'toggle' => 'tooltip',
                                ],
                                'title' => 'Открыть заказ',
                            ]) . ' ' . Html::a('<i class="fa fa-trash-o" aria-hidden="true"></i>', ['delete', 'id' => $data->id], [
                                'class' => 'btn btn-danger btn-sm',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                    'toggle' => 'tooltip',
                                ],
                                'title' => 'Удалить заказ',
                    ]);
                },
                'format' => 'raw',
            ],
            
            //'name',
            //'email:email',
            //'phone',
            //'address',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
