<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
$orderItems = $model->orderItems;
?>

<div class="order-form">
    <h3>Имя: <?= $model->name ?></h3>
    <h3>Email: <?= $model->email ?></h3>
    <h3>Телефон: <?= $model->phone ?></h3>
    <h3>Адрес: <?= $model->address ?></h3>
    <div class="table-responsive">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th scope="col">Фото</th>
                    <th scope="col">Название</th>
                    <th scope="col">Цена</th>
                    <th scope="col">Количество</th>
                    <th scope="col">Сумма</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($orderItems as $orderItem): ?>
                    <tr>
                        <?php $images = $orderItem->product->getImage(); ?>
                        <td><img src="/web/<?= $images->getPath('300x300') ?>" width="80px" alt="" class="img-fluid"></td>
                        <td><?= $orderItem->name ?></td>
                        <td><?= floor($orderItem->price / 100) . ' руб. ' . ($orderItem->price - ((floor($orderItem->price / 100)) * 100)) . ' коп.' ?></td>
                        <td><?= $orderItem->qty_item ?></td>
                        <td><?= floor($orderItem->sum_item / 100) . ' руб. ' . ($orderItem->sum_item - ((floor($orderItem->sum_item / 100)) * 100)) . ' коп.' ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="4">Итого: </td>
                    <td><?= $model->qty ?> шт.</td>
                </tr>
                <tr>
                    <td colspan="4">На сумму: </td>
                    <td><?= floor($model->sum / 100) . ' руб. ' . ($model->sum - ((floor($model->sum / 100)) * 100)) . ' коп.' ?></td>
                </tr>
            </tbody>
        </table>
    </div>


    <?php $form = ActiveForm::begin(); ?>

    <?=
    $form->field($model, 'status')->label('Закрыть заказ')->checkbox([
        'data' => [
            'toggle' => 'toggle',
            'on' => 'Закрыт',
            'off' => 'Открыт',
        ]
    ])
    ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
