<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MenuTop */

$this->title = 'Создать пункт меню';
$this->params['breadcrumbs'][] = ['label' => 'Верхнее меню', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$model->weight = \app\models\MenuTop::find()->where(['parent_id' => 0])->count();
?>
<div class="menu-top-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
