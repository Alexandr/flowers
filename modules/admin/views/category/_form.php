<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
$map = \app\models\Category::find()->indexBy('id')->orderBy('weight')->asArray()->all();
$treeSelect = new \alex290\treeselect\TreeSelect();

$this->registerJsFile(
    '@web/js/translitCyr.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJs(<<<JS
	$('.input-in').keyup(function(){
		$('.input-out').val(cyrillicToTranslit().transform($(this).val()));
	});
JS
);
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'class' => 'input-in form-control']) ?>

    <?= $form->field($model, 'parent_id')->dropDownList(yii\helpers\ArrayHelper::merge(['0' => 'Основной'], $treeSelect->getTree($map))) ?>

    <?= $form->field($model, 'weight')->textInput() ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => true, 'class' => 'input-out form-control']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
