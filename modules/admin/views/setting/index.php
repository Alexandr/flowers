<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerJsFile(
        '@web/web/lib/ckeditor/ckeditor.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerJs(<<<JS
    $('.text-full').each(function(index, el) {
        var textName = $(this).attr('name');
        CKEDITOR.replace(textName, {
            filebrowserBrowseUrl: '/web/lib/elfinder/G90hDOBX4hskhzzFn6l68M5C66bysAQH.html', // eg. 'includes/elFinder/elfinder-cke.html'
        });
    });
JS
);

$this->title = 'Настройка сайта';
?>
<div class="setting-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="setting-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'email_admin')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'text_header')->textarea(['class' => 'text-full']) ?>

        <?= $form->field($model, 'front_text_block')->textarea(['class' => 'text-full']) ?>
        
        <?= $form->field($model, 'contact')->textarea(['class' => 'text-full']) ?>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
