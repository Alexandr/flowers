<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model app\models\Article */
/* @var $form yii\widgets\ActiveForm */
$map = app\models\Category::find()->indexBy('id')->orderBy('weight')->asArray()->all();
$treeSelect = new \alex290\treeselect\TreeSelect();

$this->registerJsFile(
        '@web/js/translitCyr.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJs(<<<JS
	$('.input-in').keyup(function(){
		$('.input-out').val(cyrillicToTranslit().transform($(this).val()));
	});
JS
);

$this->registerJsFile(
        '@web/web/lib/ckeditor/ckeditor.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerJs(<<<JS
    $('.text-full').each(function(index, el) {
        var textName = $(this).attr('name');
        CKEDITOR.replace(textName, {
            filebrowserBrowseUrl: '/web/lib/elfinder/G90hDOBX4hskhzzFn6l68M5C66bysAQH.html', // eg. 'includes/elFinder/elfinder-cke.html'
        });
    });
JS
);
?>

<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'class' => 'input-in form-control']) ?>

    <?= $form->field($model, 'category_id')->dropDownList($treeSelect->getTree($map), ['prompt' => 'Выберите категорию']) ?>

    <?=
    $form->field($model, 'date_create')->widget(DateControl::classname(), [
        'displayFormat' => 'php:d.m.Y',
        'type' => DateControl::FORMAT_DATE
    ])
    ?>

    <?= $form->field($model, 'user_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'body')->textarea(['class' => 'text-full']) ?>

    <?= $form->field($model, 'frontpage')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'alias')->textInput(['maxlength' => true, 'class' => 'input-out form-control']) ?>

    <?= $form->field($model, 'count')->hiddenInput()->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
