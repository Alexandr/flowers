<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tovar */

$this->title = 'Изменить товар: '. $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tovars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
$rubl = floor($model->price/100);
$kop = ($model->price - $rubl*100);
$model->priceRub = $rubl;
$model->priceKop = $kop;
?>
<div class="tovar-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
