<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Tovar */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tovars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$rubl = floor($model->price / 100);
$kop = ($model->price - $rubl * 100);
$gall = $model->getImages();
?>
<div class="tovar-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'description:ntext',
            [
                'attribute' => 'price',
                'value' => $rubl . ' руб. ' . $kop . ' коп. ',
            ]
        ],
    ])
    ?>
    <div class="row">
        <?php foreach ($gall as $imageS): ?>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                <img src="/web/<?= $imageS->getPath('500x') ?>" alt="" class="img-responsive img-fluid">
            </div>
        <?php endforeach; ?>
    </div>

</div>
