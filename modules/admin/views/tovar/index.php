<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TovarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товар';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tovar-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'imageFiles',
                'value' => function ($data) {
                    return '<img src="/web/'. $data->getImage()->getPath('100x100') .'" alt="">';
                },
                'format' => 'raw',
            ],
            'name',
            'date_create:date',
            [
                'attribute' => 'price',
                'value' => function ($data) {
                    return floor($data->price/100).' руб. '.($data->price - (floor($data->price/100)*100)).' коп. ';
                }
            ],
            'frontpage:boolean',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
</div>
