<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datecontrol\DateControl;
use alex290\gallery\Gallery;

/* @var $this yii\web\View */
/* @var $model app\models\Tovar */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile(
        '/web/js/admjs.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>

<div class="tovar-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?=
    $form->field($model, 'date_create')->widget(DateControl::classname(), [
        'displayFormat' => 'php:d.m.Y',
        'type' => DateControl::FORMAT_DATE
    ])
    ?>

    <?= $form->field($model, 'price')->hiddenInput()->label(false) ?>
    <div class="row">
        <h4>Цена</h4>
        <div class="col-xl-2 col-lg-2 col-md-3 col-sm-6">
            <?= $form->field($model, 'priceRub')->textInput()->label('руб.') ?>
        </div>
        <div class="col-xl-2 col-lg-2 col-md-3 col-sm-6">
            <?= $form->field($model, 'priceKop')->textInput()->label('коп.') ?>
        </div>
    </div>

    <?=
    $form->field($model, 'frontpage')->checkbox(['data' =>
        [
            'toggle' => 'toggle',
        ]
    ])
    ?>
    <h3></h3>
    <h3>Галерея</h3>
    <?php if (!$model->isNewRecord): ?>

        <div class="row">
            <?= Gallery::widget(['modelsImages' => $model]) ?>  
        </div>

    <?php endif; ?>

    <?= $form->field($model, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'upload/images/*']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
