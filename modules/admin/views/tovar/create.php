<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tovar */

$this->title = 'Create Tovar';
$this->params['breadcrumbs'][] = ['label' => 'Tovars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$model->priceKop = 0;
$model->date_create = date('U');
$model->frontpage = 0;
$model->count = 0;
?>
<div class="tovar-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
