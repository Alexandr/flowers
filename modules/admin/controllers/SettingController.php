<?php

namespace app\modules\admin\controllers;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\models\Setting;

/**
 * SettingController implements the CRUD actions for Setting model.
 */
class SettingController extends Controller
{
    
    /**
     * Lists all Setting models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = $this->findModel(1);
        
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'model' => $model,]);
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }



    protected function findModel($id)
    {
        if (($model = Setting::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
