+ function($) {
    $('#tovar-pricerub').keyup(function(event) {
        var rub = Math.floor($('#tovar-pricerub').val() * 100);
        var cop = Math.floor($('#tovar-pricekop').val());
        $("#tovar-price").val(rub + cop);
    });
    $('#tovar-pricekop').keyup(function(event) {
        var rub = Math.floor($('#tovar-pricerub').val() * 100);
        var cop = Math.floor($('#tovar-pricekop').val());
        $("#tovar-price").val(rub + cop);
    });
}(jQuery);