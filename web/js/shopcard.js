+ function($) {

    cartNumb();
    showCard();

    $('.add-too-cart').on('click', function(event) {
        event.preventDefault();

        var id = $(this).data('id');
        $.ajax({
                url: '/cart/add',
                type: 'GET',
                data: { id: id },
            })
            .done(function(res) {
                cartNumb();
            })
            .fail(function() {
                console.log("error");
            });

    });

    $('.add-too-cart-numb').on('click', function(event) {
        event.preventDefault();

        var id = $(this).data('id');
        var numb = $('.numb-tovar').val();
        $('.add-too-cart-numb').addClass('disabled');
        $.ajax({
                url: '/cart/addnumb',
                type: 'GET',
                data: { id: id, numb: numb },
            })
            .done(function(res) {
                cartNumb();
            })
            .fail(function() {
                console.log("error");
            });

    });
}(jQuery);


function cartNumb() {
    $.ajax({
            url: '/cart/numb',
            type: 'GET',
        })
        .done(function(res) {
            $('.show-cart-numb').html(res);
        })
        .fail(function() {
            console.log("error");
        });

}

function clearCard() {
    $.ajax({
            url: '/cart/clear',
            type: 'GET',
        })
        .done(function() {
            cartNumb();
            showCard();
        })
        .fail(function() {
            console.log("error");
        });

}

function showCard() {
    $.ajax({
            url: '/cart/show',
            type: 'GET',
        })
        .done(function(res) {
            $('.show-card-tabl').html(res);
        })
        .fail(function() {
            console.log("error");
        });

}

function delItem(id) {
    $.ajax({
            url: '/cart/delitem',
            type: 'GET',
            data: { id: id },
        })
        .done(function(res) {
            cartNumb();
            showCard();
        })
        .fail(function() {
            console.log("error");
        });

}