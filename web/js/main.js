+ function($) {
    $('a.gallery').colorbox({
        maxHeight: "90%"
    });
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
    priceSumm();

    $('.numb-tovar').keyup(function(event) {
        priceSumm();
    });

    


}(jQuery);

$(window).on('load', function() {
    $('.flexslider.tovar').flexslider({
        animation: "slide"
    });
});

// Получение суумы изходя из количества
function priceSumm() {
    var price = $('.price-hiden').text();
    var summ = $('.numb-tovar').val();
    var priceS = price * summ;
    if (priceS > 0) {
        var rub = Math.ceil(priceS / 100);
        var cop = (priceS - (rub * 100));
        $('.add-too-cart-numb').removeClass('disabled');
        $('.price-sum').html(rub + ' руб. ' + cop + ' коп.');
    } else {
        $('.add-too-cart-numb').addClass('disabled');
    }

}
