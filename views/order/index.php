<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->registerCssFile("/web/css/bootstrap-toggle.min.css");
$this->registerJsFile(
        '/web/js/bootstrap-toggle.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerJs(<<<JS
	$('#toggle-sam').bootstrapToggle({
        on: 'Да',
        off: 'Нет'
    });

    $('#toggle-sam').change(function() {
        if ($(this).prop('checked') == true) {
            $('.field-order-address').addClass('d-none');
            $('.field-order-address input').val('Самовывоз');
        } else {
            $('.field-order-address').removeClass('d-none');
            $('.field-order-address input').val('');
        }
    })
JS
);

$this->title = 'Оформление заказа';
?>
<div class="container">
    <div class="row">
        <h1 class="light-text text-center"><?= $this->title ?></h1>
        <?php if (!empty($session['cart'])): ?>
            <div class="table-responsive">
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Фото</th>
                            <th scope="col">Название</th>
                            <th scope="col">Цена</th>
                            <th scope="col">Количество</th>
                            <th scope="col">Сумма</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($session['cart'] as $id => $value): ?>
                            <tr>
                                <td><img src="/web/<?= $value['image'] ?>" width="80px" alt="" class="img-fluid"></td>
                                <td><?= $value['name'] ?></td>
                                <td><?= floor($value['price'] / 100) . ' руб. ' . ($value['price'] - ((floor($value['price'] / 100)) * 100)) . ' коп.' ?></td>
                                <td><?= $value['qty'] ?></td>
                                <?php $sumTovar = $value['qty'] * $value['price'] ?>
                                <td><?= floor($sumTovar / 100) . ' руб. ' . ($sumTovar - ((floor($sumTovar / 100)) * 100)) . ' коп.' ?></td>
                            </tr>
                        <?php endforeach; ?>
                        <tr>
                            <td colspan="4">Итого: </td>
                            <td><?= $session['cart.qty'] ?></td>
                        </tr>
                        <tr>
                            <td colspan="4">На сумму: </td>
                            <td><?= floor($session['cart.sum'] / 100) . ' руб. ' . ($session['cart.sum'] - ((floor($session['cart.sum'] / 100)) * 100)) . ' коп.' ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="float-right">
            </div>
            <?php
            $form = ActiveForm::begin([
                        'options' => ['class' => 'form-horizontal col-12'],
                    ])
            ?>
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                    <?= $form->field($order, 'name') ?>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                    <?= $form->field($order, 'email')->label('Email (необязательно)') ?>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                    <?=
                    $form->field($order, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '+7(999) 999-99-99',
                    ])
                    ?>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                    <label class="">Самовывоз</label>
                        <input type="checkbox" data-toggle="toggle" id="toggle-sam"> 
                    
                    <?= $form->field($order, 'address') ?>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <?= Html::submitButton('Заказать', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
            <?php ActiveForm::end() ?>
        <?php else: ?>
            <h3>Корзина пуста</h3>

        <?php endif; ?>
    </div>
    <div class="row">

    </div>
</div>
