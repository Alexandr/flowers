<?php

use yii\helpers\Url;


$this->title = $model->name;
$images = $model->getImages();

?>
<div class="container">

    <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-5 col-sm-6 col-12">
            <div class="flexslider tovar">
                <ul class="slides">
                    <?php foreach ($images as $img): ?>
                        <li>
                            <a href="/web/<?= $img->getPath() ?>" class="gallery">
                                <img src="/web/<?= $img->getPath('500x500') ?>" alt="">
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>

        <div class="col-xl-8 col-lg-8 col-md-7 col-sm-6 col-12">
            <h1 class="light-text"><?= $this->title ?></h1>
            <h4 class="light-text"><?= $model->description ?></h4>
            <h2 class="light-text danger">Цена: <?= floor($model->price / 100) . ' руб. ' . ($model->price - (floor($model->price / 100) * 100)) . ' коп.' ?></h2>
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-success text-white" id="basic-addon1">Количество</span>
                        </div>
                        <input type="text" class="form-control numb-tovar" value="1">
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                    <div class="price-hiden"><?= $model->price ?></div>
                    <h4 class="price-sum success"></h4>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="add-cart-front">
                        <a href="<?= Url::to(['cart/add', 'id' => $model->id]) ?>" data-id="<?= $model->id ?>" class="add-too-cart-numb btn btn-large btn-outline-success"><i class="fas fa-cart-plus"></i> Добавить в корзину</a>
                    </div>
                </div>     
            </div>
        </div>
    </div>
    

</div>