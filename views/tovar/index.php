<?php

use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = 'Товары';
?>

<div class="container">
    <h1 class="light-text text-center"><?= $this->title ?></h1>
    <div class="row">
        <?php foreach ($models as $tovar): ?>
            <?php
            $image = $tovar->getImage();
            $nameTovar = $tovar->name;
            $price = floor(($tovar->price) / 100) . ' руб.';
            ?>
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12" data-toggle="tooltip" data-html="true" title='<?= $nameTovar ?>'>
                <a href="<?= Url::to(['/tovar/views', 'id' => $tovar->id]) ?>">
                    <img src="/web/<?= $image->getPath('500x500') ?>" alt="" class="img-fluid">
                </a>
                <div class="title-tov-gall">
                    <a href="<?= Url::to(['/tovar/views', 'id' => $tovar->id]) ?>">
                        <h4 class="light-text text-center"><?= $nameTovar ?></h4>
                    </a>
                </div>
                <div class="price-front">
                    <h4 class="light-text text-center text-danger"><?= $price ?></h4>
                </div>
                <div class="add-cart-front text-center">
                    <a href="<?= Url::to(['cart/add', 'id' => $tovar->id]) ?>" data-id="<?= $tovar->id ?>" class="add-too-cart btn btn-large btn-outline-success"><i class="fas fa-cart-plus"></i> Добавить в корзину</a>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="row">
        <?=
        LinkPager::widget([
            'pagination' => $pages,
        ]);
        ?>
    </div>
</div>