<?php

use yii\helpers\Url;
use yii\helpers\StringHelper;
use yii\widgets\LinkPager;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->title = 'Поиск по слову ' . $q;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <h1 class="light-text text-center"><?= $this->title ?></h1>
    <div class="row">
        <?php foreach ($modelTovar as $tovar): ?>
            <?php
            $image = $tovar->getImage();
            $nameTovar = $tovar->name;
            $price = floor(($tovar->price) / 100) . ' руб.';
            ?>
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12" data-toggle="tooltip" data-html="true" title='<?= $nameTovar ?>'>
                <a href="<?= Url::to(['/tovar/views', 'id' => $tovar->id]) ?>">
                    <img src="/web/<?= $image->getPath('500x500') ?>" alt="" class="img-fluid">
                </a>
                <div class="title-tov-gall">
                    <a href="<?= Url::to(['/tovar/views', 'id' => $tovar->id]) ?>">
                        <h4 class="light-text text-center"><?= $nameTovar ?></h4>
                    </a>
                </div>
                <div class="price-front">
                    <h4 class="light-text text-center text-danger"><?= $price ?></h4>
                </div>
                <div class="add-cart-front text-center">
                    <a href="<?= Url::to(['cart/add', 'id' => $tovar->id]) ?>" data-id="<?= $tovar->id ?>" class="add-too-cart btn btn-large btn-outline-success"><i class="fas fa-cart-plus"></i> Добавить в корзину</a>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <hr>
    <div class="row">
        <?php foreach ($models as $article): ?>
            <div class="col-12">
                <div class="w-100">
                    <a href="<?= Url::to(['/article/view', 'id' => $article->id]) ?>" class="text-dark">
                        <h2 class=""><?= $article->name ?> </h2>
                    </a>
                </div>
                <div class="content">
                    <?= StringHelper::truncateWords(strip_tags($article->body, '<p>'), 300) ?>
                </div>
                <div class="content">
                    <span> <i class="far fa-user"></i> <?= $article->user->first_name ?> </span>
                    <span> <i class="far fa-eye-slash"></i> <?= $article->count ?></span>
                    <span><a href="<?= Url::to(['/article/view', 'id' => $article->id]) ?>" class="readmore"><b>Подробнее</b> <i class="far fa-play-circle"></i> </a></span>
                </div>
                <hr>
            </div>

        <?php endforeach; ?>
    </div>
    <div class="row">
        <?php
        echo LinkPager::widget([
            'pagination' => $pages,
        ]);
        ?>
    </div>
</div>


