<?php
/* @var $this yii\web\View */
use yii\helpers\Url;

$this->title = 'Цветы, подарки в Усть-Ордынском Флора+';
$tovars = \app\models\Tovar::find()->orderBy(['date_create'=> SORT_ASC])->where(['frontpage'=>1])->limit(8)->all();

/* $this->registerJsFile(
    '/web/js/shopcard.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);*/

use app\models\Setting;

$setting = Setting::find()->one();

?>
<div class="header-wrap">
    <div class="container">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-white">
            <?= $setting->text_header ?>
        </div>
    </div>
</div>
<div class="container">
    <h1 class="text-center mb-30">Продукция</h1>
    <div class="row">
        <?php foreach ($tovars as $tovar): ?>
            <?php
            $image = $tovar->getImage();
            $nameTovar = $tovar->name;
            $price = floor(($tovar->price) / 100) . ' руб.';
            ?>
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12" data-toggle="tooltip" data-html="true" title='<?= $nameTovar ?>'>
                <a href="<?= Url::to(['/tovar/views', 'id' => $tovar->id]) ?>">
                    <img src="/web/<?= $image->getPath('500x500') ?>" alt="" class="img-fluid">
                </a>
                <div class="title-tov-gall">
                    <a href="<?= Url::to(['/tovar/views', 'id' => $tovar->id]) ?>">
                    <h4 class="light-text text-center"><?= $nameTovar ?></h4>
                    </a>
                </div>
                <div class="price-front">
                    <h4 class="light-text text-center text-danger"><?= $price ?></h4>
                </div>
                <div class="add-cart-front text-center">
                    <a href="<?= Url::to(['cart/add', 'id'=>$tovar->id]) ?>" data-id="<?= $tovar->id ?>" class="add-too-cart btn btn-large btn-outline-success"><i class="fas fa-cart-plus"></i> Добавить в корзину</a>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<div class="moto-widget moto-widget-container moto-background-fixed moto-container_content_576d463d" data-widget="container" data-container="container" data-css-name="moto-container_content_576d463d">
    <div class="moto-widget moto-widget-container moto-container_content_5773d0ab" data-widget="container" data-container="container" data-css-name="moto-container_content_5773d0ab">
        <div class="moto-widget moto-widget-row row-fixed" data-widget="row">
            <div class="container-fluid">
                <div class="row">
                    <div class="moto-cell col-sm-2" data-container="container"></div>
                    <div class="moto-cell col-sm-8" data-container="container">
                        <div class="moto-widget moto-widget-spacer moto-preset-default moto-spacing-top-large moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto" data-widget="spacer" data-preset="default" data-spacing="lama">
                            <div class="moto-widget-spacer-block" style="height: 0px;"></div>
                        </div>
                        <div class="moto-widget moto-widget-spacer moto-preset-default moto-spacing-top-medium moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" data-widget="spacer" data-preset="default" data-spacing="maaa">
                            <div class="moto-widget-spacer-block" style="height: 5px;"></div>
                        </div>
                        <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-medium moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="masa">
                            <?= $setting->front_text_block ?>
                        </div>
                        <div class="moto-widget moto-widget-spacer moto-preset-default moto-spacing-top-medium moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="spacer" data-preset="default" data-spacing="masa">
                            <div class="moto-widget-spacer-block" style="height: 0px;"></div>
                        </div>
                        <div class="moto-widget moto-widget-spacer moto-preset-default moto-spacing-top-large moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto" data-widget="spacer" data-preset="default" data-spacing="lama">
                            <div class="moto-widget-spacer-block" style="height: 0px;"></div>
                        </div>
                    </div>
                    <div class="moto-cell col-sm-2" data-container="container"></div>
                </div>
            </div>
        </div>
    </div>
</div>
