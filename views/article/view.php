<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Article */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => $model->category->name, 'url' => yii\helpers\Url::to(['/category/view', 'id'=>$model->category_id])];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">

    <h1 class="light-text text-center"><?= Html::encode($this->title) ?></h1>
    
    <div class="content">
        <?= $model->body ?>
    </div>
</div>
