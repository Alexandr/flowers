<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\SearchForm;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use app\models\Setting;

$modelSearch = new SearchForm();

$setting = Setting::find()->one();

AppAsset::register($this);
$this->registerCssFile(
        '/web/css/bootstrap.css'
);
$this->registerJsFile(
        '/web/js/popper.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);
$this->registerJsFile(
        '/web/js/bootstrap.js', ['depends' => [\yii\web\JqueryAsset::className()]]
);

$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => Url::to(['/web/favicon.png'])]);

$map = app\models\MenuTop::find()->indexBy('id')->orderBy('weight')->asArray()->all();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <header>
                <div class="bg-light panel-top">
                    <div class="container">

                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-sm-12">
                                <?php $searchForm = ActiveForm::begin(['options' => ['class' => 'form-inline']]); ?>
                                <?= $searchForm->field($modelSearch, 'q')->textInput(['class' => 'form-control'])->label('') ?>
                                <?= Html::submitButton('<i class="fas fa-search"></i>', ['class' => 'btn btn-outline-success']) ?>
                                <?php ActiveForm::end(); ?>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-sm-12">
                                <ul class="nav justify-content-end">
                                    <li class="show-cart-numb"></li>
                                    <?php if (!Yii::$app->getUser()->isGuest): ?>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle text-success" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-user fa-2x"></i><b class="caret"></b>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                                <?php if (Yii::$app->getUser()->identity->username == 'admin'): ?>
                                                    <a class="dropdown-item" href="<?= Url::to('/admin/') ?>" data-method="post"><i class="fas fa-unlock-alt"></i> Админ панель</a>
                                                <?php endif; ?>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="<?= Url::to('/site/logout') ?>" data-method="post"><i class="fas fa-sign-out-alt"></i> Выйти</a>
                                            </div>
                                        <?php endif; ?>
                                        <?php if (Yii::$app->getUser()->isGuest): ?>
                                        <li  class="nav-item"><a class=" nav-link text-success" href="<?= Url::to('/site/login') ?>"><i class="fas fa-sign-in-alt fa-2x"></i></a></li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div> 
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <div class="container">
                        <a class="navbar-brand" href="/"><h1 class="logotype danger"><img src="/web/images/mt-471_home-logo1.png" alt=""> Флора+</h1></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse justify-content-end nav-link-x2" id="navbarSupportedContent">
                            <?= \alex290\treemenu\MenuTree::widget(['arrMenu' => $map]); ?>
                        </div>
                    </div>
                </nav>
            </header>
            <div class="container-fluid">
                <div class="row">
                    <div class="container">
                        <nav aria-label="breadcrumb">
                            <?=
                            Breadcrumbs::widget([
                                'itemTemplate' => '<li class="breadcrumb-item">{link}</li>', // template for all links
                                'activeItemTemplate' => '<li class="breadcrumb-item active">{link}</li>', // template for all links
                                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                            ])
                            ?>
                        </nav>
                        <?= Alert::widget() ?>
                    </div>
                </div>
                <div class="row">
                    <div class="w-100">
                        <?= $content ?>
                    </div>
                </div>
            </div>
        </div>

        <footer id="section-footer" class="footer moto-section" data-widget="section" data-container="section" data-moto-sticky="{mode:'smallHeight', direction:'bottom'}">
            <div class="moto-widget moto-widget-container moto-container_footer_576c3288" data-widget="container" data-container="container" data-css-name="moto-container_footer_576c3288" data-draggable-disabled="">
                <div data-widget-id="wid__spacer__5a93f19445e67" class="moto-widget moto-widget-spacer moto-preset-default moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto " data-widget="spacer" data-preset="default" data-spacing="sasa" data-visible-on="mobile-v">
                    <div class="moto-widget-spacer-block" style="height:0px"></div>
                </div>
                <div class="moto-widget moto-widget-row row-fixed" data-widget="row">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="moto-cell col-sm-3" data-container="container">
                                <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-medium moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="masa">
                                    <div class="moto-widget-text-content moto-widget-text-editable">
                                        <p class="moto-text_system_9">Категории</p>
                                    </div>
                                </div>
                                <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="sasa">
                                    <div class="moto-widget-text-content moto-widget-text-editable">
                                        <p class="moto-text_system_10">
                                            <a data-action="store.category" data-id="7" class="moto-link" href="<?= Url::to(['category/view', 'id' => 1]) ?>" data-keyword="corporate">Информация</a>
                                            ​
                                            <br></p>
                                    </div>
                                </div>
                                <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-auto moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="aasa">
                                    <div class="moto-widget-text-content moto-widget-text-editable">
                                        <p class="moto-text_system_10">
                                            <a data-action="store.category" data-id="8" class="moto-link" href="<?= Url::to(['category/view', 'id' => 2]) ?>" data-keyword="love-romance">Новости</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="moto-cell col-sm-3" data-container="container">
                                <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-medium moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="masa">
                                    <div class="moto-widget-text-content moto-widget-text-editable">
                                        <p class="moto-text_system_9">Информация</p>
                                    </div>
                                </div>
                                <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="sasa">
                                    <div class="moto-widget-text-content moto-widget-text-editable">
                                        <p class="moto-text_system_10">
                                            <a href="/about/" data-action="page" data-id="45" class="moto-link"></a>
                                        </p>
                                    </div>
                                </div>

                            </div>
                            <div class="moto-cell col-sm-3" data-container="container">
                                <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-medium moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="masa">
                                    <div class="moto-widget-text-content moto-widget-text-editable">
                                        <p class="moto-text_system_9">Наши контакты</p>
                                    </div>
                                </div>
                                <div class="moto-widget moto-widget-row row-gutter-0" data-widget="row" data-grid-type="xs">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="moto-cell col-xs-2" data-container="container">
                                                <div data-widget-id="wid__image__5a93f19447188" class="moto-widget moto-widget-image moto-preset-default  moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto  " data-widget="image">
                                                    <span class="moto-widget-image-link">
                                                        <img data-src="images/mt-471_home_footer-icon1.png" class="moto-widget-image-picture lazyload" data-id="1163" title="" alt=""></span>
                                                </div>
                                            </div>
                                            <div class="moto-cell col-xs-10" data-container="container">
                                                <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="saaa">
                                                    <div class="moto-widget-text-content moto-widget-text-editable">
                                                        <?= $setting->contact ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="moto-cell col-sm-3" data-container="container">
                                <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-medium moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="masa">
                                    <div class="moto-widget-text-content moto-widget-text-editable">
                                        <p class="moto-text_system_9">Социальные сети</p>
                                    </div>
                                </div>
                                <ul class="social">
                                    <li>
                                        <a href="https://vk.com/club131025196" class="circle text-white bg-secondary" target="blank">
                                            <i class="fab fa-vk"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-widget-id="wid__spacer__5a93f19447b77" class="moto-widget moto-widget-spacer moto-preset-default moto-spacing-top-medium moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto " data-widget="spacer" data-preset="default" data-spacing="maaa" data-visible-on="mobile-v">
                    <div class="moto-widget-spacer-block" style="height:5px"></div>
                </div>
                <div data-widget-id="wid__divider_horizontal__5a93f19447ed5" class="moto-widget moto-widget-divider moto-preset-default moto-align-left moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-small moto-spacing-left-auto  " data-widget="divider_horizontal" data-preset="default">
                    <hr class="moto-widget-divider-line" style="max-width:100%;width:100%;"></div>
                <div class="moto-widget moto-widget-row row-fixed" data-widget="row" data-draggable-disabled="">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="moto-cell col-sm-12" data-container="container">
                                <div class="moto-widget moto-widget-text moto-preset-default moto-spacing-top-medium moto-spacing-right-auto moto-spacing-bottom-medium moto-spacing-left-auto" data-widget="text" data-preset="default" data-spacing="mama" data-draggable-disabled="">
                                    <div class="moto-widget-text-content moto-widget-text-editable">
                                        <p class="moto-text_118">
                                            All Rights Reserved © 2018. Designed by
                                            <a href="http://webgoal.ru/ target="_blank" rel="nofollow" data-action="url" class="moto-link">WebGoal</a>
                                            <br></p>
                                    </div>
                                </div>
                                <div data-widget-id="wid__spacer__5a93f1944802c" class="moto-widget moto-widget-spacer moto-preset-default moto-spacing-top-small moto-spacing-right-auto moto-spacing-bottom-auto moto-spacing-left-auto " data-widget="spacer" data-preset="default" data-spacing="saaa" data-visible-on="mobile-v">
                                    <div class="moto-widget-spacer-block" style="height:0px"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
