<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Category */

$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">

    <h1 class="light-text"><?= Html::encode($this->title) ?></h1>
    <hr>
    <div class="row">
        <?php foreach ($pagesModels as $article): ?>
            <div class="col-12">
                <div class="w-100">
                    <a href="<?= Url::to(['/article/view', 'id' => $article->id]) ?>" class="text-dark">
                        <h2 class=""><?= $article->name ?> </h2>
                    </a>
                </div>
                <div class="content">
                    <?= StringHelper::truncateWords(strip_tags($article->body, '<p>'), 300) ?>
                </div>
                <div class="content">

                    <span> <i class="far fa-eye-slash"></i> <?= $article->count ?></span>
                    <span><a href="<?= Url::to(['/article/view', 'id' => $article->id]) ?>" class="readmore"><b>Подробнее</b> <i class="far fa-play-circle"></i> </a></span>
                </div>
                <hr>
            </div>

        <?php endforeach; ?>
    </div>
    <div class="row">
        <?php
        echo LinkPager::widget([
            'pagination' => $pages,
        ]);
        ?>
    </div>


</div>
