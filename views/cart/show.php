<?php if (!empty($session['cart'])): ?>
    <div class="table-responsive">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th scope="col">Фото</th>
                    <th scope="col">Название</th>
                    <th scope="col">Цена</th>
                    <th scope="col">Количество</th>
                    <th scope="col"><i class="far fa-trash-alt danger del-item-all" onclick="clearCard()"></i></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($session['cart'] as $id => $value): ?>
                    <tr>
                        <td><img src="/web/<?= $value['image'] ?>" width="150px" alt="" class="img-fluid"></td>
                        <td><?= $value['name'] ?></td>
                        <td><?= floor($value['price'] / 100) . ' руб. ' . ($value['price'] - ((floor($value['price'] / 100)) * 100)) . ' коп.' ?></td>
                        <td><?= $value['qty'] ?></td>
                        <td><i class="far fa-trash-alt danger del-item" data-id="<?= $id ?>" onclick="delItem(<?= $id ?>)"></i></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="4">Итого: </td>
                    <td><?= $session['cart.qty'] ?></td>
                </tr>
                <tr>
                    <td colspan="4">На сумму: </td>
                    <td><?= floor($session['cart.sum'] / 100) . ' руб. ' . ($session['cart.sum'] - ((floor($session['cart.sum'] / 100)) * 100)) . ' коп.' ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="float-right">
    </div>
<?php else: ?>
    <h3>Корзина пуста</h3>

<?php endif; ?>
