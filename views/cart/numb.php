<?php
use yii\helpers\Url;
?>
<?php if (!empty($session['cart'])): ?>
<a class=" nav-link text-success" href="<?= Url::to('/cart/view') ?>"><i class="fas fa-shopping-cart fa-2x"></i> (<?= $session['cart.qty'] ?>)</li></a>
<?php else: ?>
<a class="nav-link text-success"><i class="fas fa-shopping-cart fa-2x"></i> <li class="show-cart-numb"></li></a>
<?php endif; ?>