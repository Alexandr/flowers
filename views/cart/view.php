<?php
$this->title = 'Корзина';

?>

<div class="container">
    <h2 class="light-text text-center"><?= $this->title ?></h2>
    <div class="show-card-tabl">
        
    </div>
    <div class="float-right">
        <a href="<?= yii\helpers\Url::to('/order') ?>" class="btn btn-success">Оформить заказ</a>
        <button type="button" class="btn btn-danger" onclick="clearCard()">Очистить корзину</button>
    </div>
</div>
