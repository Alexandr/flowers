
<div style="float: left; width: 100%; margin-top: 10px;">
    <table style="width: 100%; border: 1px solid #ddd; border-collapse: collapse;">
        <thead>
            <tr style="background: #f9f9f9;">
                <th style="padding: 8px; border: 1px solid #ddd;">Название</th>
                <th style="padding: 8px; border: 1px solid #ddd;">Цена</th>
                <th style="padding: 8px; border: 1px solid #ddd;">Количество</th>
                <th style="padding: 8px; border: 1px solid #ddd;">Сумма</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($session['cart'] as $id => $value): ?>
                <tr>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= $value['name'] ?></td>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= floor($value['price'] / 100) . ' руб. ' . ($value['price'] - ((floor($value['price'] / 100)) * 100)) . ' коп.' ?></td>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= $value['qty'] ?></td>
                    <?php $sumTovar = $value['qty'] * $value['price'] ?>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= floor($sumTovar / 100) . ' руб. ' . ($sumTovar - ((floor($sumTovar / 100)) * 100)) . ' коп.' ?></td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td colspan="3" style="padding: 8px; border: 1px solid #ddd;">Итого: </td>
                <td style="padding: 8px; border: 1px solid #ddd;"><?= $session['cart.qty'] ?></td>
            </tr>
            <tr>
                <td colspan="3" style="padding: 8px; border: 1px solid #ddd;">На сумму: </td>
                <td style="padding: 8px; border: 1px solid #ddd;"><?= floor($session['cart.sum'] / 100) . ' руб. ' . ($session['cart.sum'] - ((floor($session['cart.sum'] / 100)) * 100)) . ' коп.' ?></td>
            </tr>
        </tbody>
    </table>
</div>
<div style="float: left; width: 100%; margin-top: 50px;">
    <a type="button" href="http://florda.ru/admin/order/update?id=<?= $ordid ?>" style="padding: 8px; background-color: #82FF5B; margin-top: 25px; color: #000; text-decoration: none;">Открыть заказ</a>
</div>