<?php

namespace app\controllers;

use Yii;
use app\models\Category;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;
use app\models\Article;
use app\models\SearchForm;
use yii\helpers\Html;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller {
    
    public function beforeAction($param) {
        $sear = new SearchForm();
        if ($sear->load(\Yii::$app->request->post())) {
            $q = Html::encode($sear->q);
            if ($q == NULL) {
               return TRUE; 
            }
            return $this->redirect(\Yii::$app->urlManager->createUrl(['site/search', 'q' => $q]));
        }
        return TRUE;
    }

    public function actionView($id) {
        $model = $this->findModel($id);
        $query = Article::find()->where(['category_id' => $id])->orderBy(['category_id' => SORT_ASC]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $pagesModels = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();

        return $this->render('view', [
                'pagesModels' => $pagesModels,
                'pages' => $pages,
                'model' => $model,
        ]);
    }

    protected function findModel($id) {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
