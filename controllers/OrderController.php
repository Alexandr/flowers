<?php

namespace app\controllers;

use Yii;
use app\models\Order;
use app\models\OrderItems;
use app\models\Setting;
use app\models\SearchForm;
use yii\helpers\Html;



class OrderController extends \yii\web\Controller {
    
    public function beforeAction($param) {
        $sear = new SearchForm();
        if ($sear->load(\Yii::$app->request->post())) {
            $q = Html::encode($sear->q);
            if ($q == NULL) {
               return TRUE; 
            }
            return $this->redirect(\Yii::$app->urlManager->createUrl(['site/search', 'q' => $q]));
        }
        return TRUE;
    }

    public function actionIndex() {
        $session = Yii::$app->session;
        $session->open();
        $order = new Order();
        $setting = Setting::find()->one();
        if ($order->load(\Yii::$app->request->post())) {
            $order->qty = $session['cart.qty'];
            $order->sum = $session['cart.sum'];
            $order->created_at = date('U');
            $order->updated_at = date('U');
            $order->status = 0;

            if ($order->save()) {
                $this->saveOrderItems($session['cart'], $order->id);
                \Yii::$app->mailer->compose('order', ['session' => $session, 'ordid' => $order->id])
                        ->setFrom(['info@florda.ru' => 'Магазин цветов'])
                        ->setTo($setting->email_admin)
                        ->setSubject('Заказ товара')
                        ->send();
                $session->remove('cart');
                $session->remove('cart.qty');
                $session->remove('cart.sum');
                return $this->redirect('/order/success');
            }
        }
        return $this->render('index', [
                'session' => $session,
                'order' => $order,
        ]);
    }

    public function actionSuccess() {
        return $this->render('success');
    }

    protected function saveOrderItems($items, $order_id) {
        foreach ($items as $id => $item) {
            $orderItems = new OrderItems();
            $orderItems->order_id = $order_id;
            $orderItems->product_id = $id;
            $orderItems->name = $item['name'];
            $orderItems->price = $item['price'];
            $orderItems->qty_item = $item['qty'];
            $orderItems->sum_item = $item['price'] * $item['qty'];
            $orderItems->save();
        }
    }

}
