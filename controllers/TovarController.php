<?php

namespace app\controllers;
use yii\data\Pagination;

use app\models\Tovar;
use app\models\SearchForm;
use yii\helpers\Html;

class TovarController extends \yii\web\Controller {
    
    public function beforeAction($param) {
        $sear = new SearchForm();
        if ($sear->load(\Yii::$app->request->post())) {
            $q = Html::encode($sear->q);
            if ($q == NULL) {
               return TRUE; 
            }
            return $this->redirect(\Yii::$app->urlManager->createUrl(['site/search', 'q' => $q]));
        }
        return TRUE;
    }

    public function actionViews($id) {
        
        $model = $this->findModel($id);
        $model->count ++;
        $model->save();
        return $this->render('views', [
                    'model' => $model,
        ]);
    }

    public function actionIndex() {
        $query = Tovar::find()->orderBy(['date_create' => SORT_ASC]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $models = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();

        return $this->render('index', [
               'models' => $models,
               'pages' => $pages,
        ]);
    }

    protected function findModel($id) {
        if (($model = Tovar::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
