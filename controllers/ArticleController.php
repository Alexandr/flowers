<?php

namespace app\controllers;

use Yii;
use app\models\Article;
use app\models\ArticleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\models\SearchForm;
use yii\helpers\Html;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends Controller
{
    public function beforeAction($param) {
        $sear = new SearchForm();
        if ($sear->load(\Yii::$app->request->post())) {
            $q = Html::encode($sear->q);
            if ($q == NULL) {
               return TRUE; 
            }
            return $this->redirect(\Yii::$app->urlManager->createUrl(['site/search', 'q' => $q]));
        }
        return TRUE;
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);
        $count = $model->count + 1;
        $model->count = $count;
        $model->save();
        return $this->render('view', [
            'model' => $model,
        ]);
    }

   
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
