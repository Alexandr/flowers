<?php

namespace app\controllers;

use Yii;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\Response;
use yii\data\Pagination;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SearchForm;
use app\models\Article;
use app\models\Tovar;

class SiteController extends Controller {

    public function beforeAction($param) {
        $sear = new SearchForm();
        if ($sear->load(\Yii::$app->request->post())) {
            $q = Html::encode($sear->q);
            if ($q == NULL) {
               return TRUE; 
            }
            return $this->redirect(\Yii::$app->urlManager->createUrl(['site/search', 'q' => $q]));
        }
        return TRUE;
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    public function actionSearch() {
        $q = Yii::$app->getRequest()->getQueryParam('q');

        $query = Article::find()->orWhere(['like', 'name', $q])->orWhere(['like', 'body', $q]);
        $queryTovar = Tovar::find()->orWhere(['like', 'name', $q])->orWhere(['like', 'description', $q]);
        if ($query->count() > $queryTovar->count()) {
            $count = clone $query;
        } else {
            $count = clone $queryTovar;
        }

        $pages = new Pagination(['totalCount' => $count->count()]);
        $models = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
        $modelTovar = $queryTovar->offset($pages->offset)
                ->limit($pages->limit)
                ->all();


        return $this->render('search', [
                'q' => $q,
                'models' => $models,
                'modelTovar' => $modelTovar,
                'pages' => $pages,
        ]);
    }

}
