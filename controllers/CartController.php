<?php

namespace app\controllers;

use app\models\Tovar;
use app\models\Cart;
use Yii;

class CartController extends \yii\web\Controller {

    public function actionAdd($id) {
        $this->layout = FALSE;
        $tovar = Tovar::findOne($id);
        if (empty($tovar)) {
            return false;
        }


        return $this->render('add', [
            'id' => $id,
            'tovar' => $tovar,
        ]);
    }
    
    public function actionAddnumb() {
        $id = Yii::$app->request->get('id');
        $numb = Yii::$app->request->get('numb');
        $this->layout = FALSE;
        $tovar = Tovar::findOne($id);
        if (empty($tovar)) {
            return false;
        }


        return $this->render('addnumb', [
            'id' => $id,
            'tovar' => $tovar,
            'numb' => $numb,
        ]);
    }

    public function actionView() {
        return $this->render('view');
    }
    
    public function actionNumb() {
        $session = Yii::$app->session;
        $session->open();
        $this->layout = false;
        return $this->render('numb', compact('session'));
    }
    public function actionShow() {
        $session = Yii::$app->session;
        $session->open();
        $this->layout = false;
        return $this->render('show', compact('session'));
    }
    
    
    public function actionClear() {
        return $this->render('clear');
    }
    
    public function actionDelitem($id) {
        return $this->render('delitem', [
            'id' => $id,
        ]);
    }

}
