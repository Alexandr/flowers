<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cart".
 *
 * @property int $id
 */
class Cart extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'cart';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
        ];
    }

    public function addToCart($tovar, $qty = 1) {
        if (isset($_SESSION['cart'][$tovar->id])) {
            $_SESSION['cart'][$tovar->id]['qty'] += $qty;
        } else {
            $_SESSION['cart'][$tovar->id] = [
                'qty' => $qty,
                'name' => $tovar->name,
                'price' => $tovar->price,
                'image' => $tovar->getImage()->getPath('500x500'),
            ];
        }
        $_SESSION['cart.qty'] = isset($_SESSION['cart.qty']) ? $_SESSION['cart.qty'] + $qty : $qty;
        $_SESSION['cart.sum'] = isset($_SESSION['cart.sum']) ? $_SESSION['cart.sum'] + $qty * $tovar->price : $qty * $tovar->price;
    }

    public function recalc($id) {
        if (!isset($_SESSION['cart'][$id])) return false;
        $qtyMinus = $_SESSION['cart'][$id]['qty'];
        $sumMinus = $_SESSION['cart'][$id]['price'] * $_SESSION['cart'][$id]['qty'];
        
        $_SESSION['cart.qty'] -= $qtyMinus;
        $_SESSION['cart.sum'] -= $sumMinus;
        unset($_SESSION['cart'][$id]);
    }

}
