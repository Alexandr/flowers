<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu_top".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property string $link
 * @property int $weight
 * @property string $attribute
 */
class MenuTop extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_top';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'name', 'link', 'weight'], 'required'],
            [['parent_id', 'weight'], 'integer'],
            [['attribute'], 'string'],
            [['name'], 'string', 'max' => 50],
            [['link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Родитель',
            'name' => 'Название',
            'link' => 'Ссылка',
            'weight' => 'Вес',
            'attribute' => 'Атрибут ссылки',
        ];
    }
}
