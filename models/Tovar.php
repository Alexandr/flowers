<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
/**
 * This is the model class for table "tovar".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $price
 */
class Tovar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tovar';
    }
    
    public $imageFiles;
    public $priceRub;
    public $priceKop;
    
    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['price'], 'required'],
            [['price', 'date_create', 'count'], 'integer'],
            [['priceRub'], 'integer'],
            [['priceKop'], 'integer'],
            [['frontpage'], 'integer', 'max' => 1],
            [['name'], 'string', 'max' => 250],
            [['imageFiles'], 'file', 'extensions' => 'png, jpg', 'maxFiles' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'description' => 'Описание',
            'date_create' => 'Дата создания',
            'price' => 'Цена',
            'count' => 'Количество просмотров',
            'priceRub' => 'Цена руб.',
            'priceKop' => 'Цена коп.',
            'imageFiles' => 'Изображение',
            'frontpage' => 'На главной',
        ];
    }
    public function upload()
    {
        if ($this->validate()) {
            
            foreach ($this->imageFiles as $file) {
                $patch = 'upload/images/'.$file->baseName. '.' . $file->extension;
                $file->saveAs($patch);
                $this->attachImage($patch);
                unlink($patch);
            }
            return true;
        } else {
            return false;
        }
    }
}
