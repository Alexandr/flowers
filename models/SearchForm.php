<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;

use yii\base\Model;

/**
 * Description of Search
 *
 * @author art
 */
class SearchForm extends Model {
    
    public $q;
    
    public function rules() {
        return [
            [['q'], 'string'],
        ];
    }
    
    
}
