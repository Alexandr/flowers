<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "setting".
 *
 * @property int $id
 * @property string $email_admin
 * @property string $text_header
 * @property string $contact
 * @property string $front_text_block
 */
class Setting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text_header', 'contact', 'front_text_block'], 'string'],
            [['email_admin'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email_admin' => 'Email администратора',
            'text_header' => 'Текст в шапке',
            'contact' => 'Контакты',
            'front_text_block' => 'Текст на главное в блоке',
        ];
    }
}
